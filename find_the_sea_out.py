

p_la_capa = "/home/fidel/GitLab/river-network-subset/data/red_hidrografica_250k.shp"

la_capa = QgsVectorLayer(p_la_capa,"rios","ogr")

staring_segments = [81076,81608]

selected_ids = []

def down_river_segment(id):
    try:
        expression = 'id = '+str(id)
        request = QgsFeatureRequest().setFilterExpression(expression)
        iter = la_capa.getFeatures(request)
        feats = [ feat for feat in iter ]
        feat = feats[0]
        from_node = feat['FNODE_']
        to_node = feat['TNODE_']
        #print(feat['id'], feat['FNODE_'], feat['TNODE_'])
        selected_ids.append(feat['id'])
        la_capa.startEditing()
        feat.setAttribute('o', 1)
        la_capa.updateFeature(feat)
        la_capa.commitChanges()
        expression = 'FNODE_ = '+str(to_node)
        request = QgsFeatureRequest().setFilterExpression(expression)
        next_iter = la_capa.getFeatures(request)
        next_feats = [ feat for feat in next_iter ]
        next_feat = next_feats[0]
        print(next_feat['id'], next_feat['FNODE_'], next_feat['TNODE_'])
        down_river_segment(next_feat['id'])
    except:
        print("ya")
    

#down_river_segment(81608)
down_river_segment(72464)
print(selected_ids)

#for f in la_capa.getFeatures()




